cmake_minimum_required (VERSION 3.8.2)
project (incotool)

set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake")

add_subdirectory (common/include)
add_subdirectory (common/dbus-helper)
add_subdirectory (common/ictprob)
add_subdirectory (common/ictcgroup)

add_subdirectory (invoker/ictexec)
add_subdirectory (invoker/ictcgroupd)
