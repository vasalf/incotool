/* MIT License
 *
 * Copyright (c) 2017 Vasiliy Alferov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <ictcgroup/ictcgroup.h>
#include <ictcgroup/ictcgroupd.h>

#include <experimental/filesystem>
#include <sstream>
#include <algorithm>
#include <memory>
#include <iostream>

#include <unistd.h>

namespace ictcgroup {

ictcgroup_exception::ictcgroup_exception(std::string method) 
        : method_(method), what_("ictcgroupd action failed: " + method) {
}

const char *ictcgroup_exception::what() const noexcept {
    return what_.c_str();
}

ictcgroup_cgroup_exception::ictcgroup_cgroup_exception(std::string reason)
        : ictcgroup_exception(""), what_(reason) {
}

const char *ictcgroup_cgroup_exception::what() const noexcept {
    return what_.c_str();
}

incotool_cgroup::incotool_cgroup(std::shared_ptr<incotool_cgroup> parent, std::string name) 
        : parent_(parent), name_(name) {
}

std::string incotool_cgroup::get_name() const {
    return name_;
}

std::string incotool_cgroup::get_module_path(std::string module) const {
    namespace fs = std::experimental::filesystem;
    return (fs::path("/sys/fs/cgroup") / fs::path(module) / fs::path(get_hierarchy_path())).string();
}

std::string incotool_cgroup::get_property_path(std::string module, std::string property) const {
    namespace fs = std::experimental::filesystem;
    return (fs::path(get_module_path(module)) / fs::path(property)).string();
}

std::string incotool_cgroup::get_hierarchy_path() const {
    namespace fs = std::experimental::filesystem;
    fs::path res("/");
    if (parent_)
        res = fs::path(parent_->get_hierarchy_path());
    res /= name_;
    return res.string();
}

incotool_root_cgroup::incotool_root_cgroup()
        : incotool_cgroup(std::shared_ptr<incotool_cgroup>(NULL), "incotool") {
}

incotool_invoke_cgroup::incotool_invoke_cgroup(std::shared_ptr<incotool_problem_cgroup> prob, std::string name)
        : incotool_cgroup(prob, name), prob_id_(prob->get_prob_id()) {
}

void incotool_invoke_cgroup::add_exec_prefix(std::string &cmd, std::vector<std::string> &args) const {
    cmd = "cgexec";
    std::reverse(args.begin(), args.end());
    args.push_back("memory,cpu:" + get_hierarchy_path());
    args.push_back("-g");
    args.push_back(cmd);
    std::reverse(args.begin(), args.end());
}

void incotool_invoke_cgroup::remove(DBus::DBusClientPtr& dbus_client) const {
    if (DBus::ictcgroupd::remove_invoke_cgroup(dbus_client, "app.incotool.ictcgroupd", "/app/incotool/ictcgroupd", prob_id_, name_))
        throw ictcgroup_exception("app.incotool.ictcgroupd.remove_invoke_cgroup");
}

int incotool_invoke_cgroup::get_prob_id() const {
    return prob_id_;
}

incotool_problem_cgroup::incotool_problem_cgroup(DBus::DBusClientPtr& dbus_client, ictprob::incotool_problem &ictprob, std::shared_ptr<incotool_root_cgroup> parent)
        : incotool_cgroup(NULL, "") {
    parent_ = parent;
    name_ = ictprob.get_prettified_name();
    prob_id_ = ictprob.get_prob_id();

    if (DBus::ictcgroupd::create_problem_cgroup(dbus_client, "app.incotool.ictcgroupd", "/app/incotool/ictcgroupd", ictprob.get_prob_id(), getuid(), 0))
        throw ictcgroup_exception("app.incotool.ictcgroupd.create_problem_cgroup");

    // We set the real memory limit to <ML + 1> so that we can determine whether all of the memory was used or not.
    if (ictprob.get_config().memory_limit)
        set_property_value<std::uint64_t>("memory", "memory.limit_in_bytes", ictprob.get_config().memory_limit + 1);
}

std::uint32_t incotool_problem_cgroup::get_prob_id() const {
    return prob_id_;
}

std::shared_ptr<incotool_invoke_cgroup> incotool_problem_cgroup::create_subgroup(DBus::DBusClientPtr& client, std::shared_ptr<incotool_problem_cgroup> prob_cgroup) const {
    std::string subgroup_name;
    if (DBus::ictcgroupd::create_invoke_cgroup(client, "app.incotool.ictcgroupd", "/app/incotool/ictcgroupd", prob_id_, getuid(), 0, subgroup_name))
        throw ictcgroup_exception("app.incotool.ictcgroupd.create_invoke_cgroup");
    return std::make_shared<incotool_invoke_cgroup>(prob_cgroup, subgroup_name);
}

std::shared_ptr<incotool_root_cgroup> get_root_cgroup() {
    return std::make_shared<incotool_root_cgroup>();
}

std::shared_ptr<incotool_problem_cgroup> create_problem_cgroup(DBus::DBusClientPtr& client, ictprob::incotool_problem ictprob, std::shared_ptr<incotool_root_cgroup> parent) {
    return std::make_shared<incotool_problem_cgroup>(client, ictprob, parent);
}

std::shared_ptr<incotool_problem_cgroup> create_problem_cgroup(DBus::DBusClientPtr& client, ictprob::incotool_problem& ictprob) {
    return std::make_shared<incotool_problem_cgroup>(client, ictprob, get_root_cgroup());
}

std::shared_ptr<incotool_invoke_cgroup> create_invoke_cgroup(DBus::DBusClientPtr& client, std::shared_ptr<incotool_problem_cgroup> prob_cgroup) {
    return std::shared_ptr<incotool_invoke_cgroup>(prob_cgroup->create_subgroup(client, prob_cgroup));
}

std::shared_ptr<incotool_invoke_cgroup> create_invoke_cgroup(DBus::DBusClientPtr& client, ictprob::incotool_problem ictprob) {
    return create_invoke_cgroup(client, create_problem_cgroup(client, ictprob));
}

}
