/* MIT License
 *
 * Copyright (c) 2017 Vasiliy Alferov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef ICTCGROUPD_H_
#define ICTCGROUPD_H_

#include <dbus-helper.h>

namespace DBus {

class ictcgroupd {
public:
    static std::uint32_t create_problem_cgroup(DBusClientPtr& client, std::string recv, std::string obj, 
                                               std::uint32_t prob_id, std::uint32_t owner, std::uint32_t group) {
        std::uint32_t ret;

        DBusMethodCallParams<std::uint32_t, std::uint32_t, std::uint32_t> in(prob_id, owner, group);
        DBusMethodCallParams<std::uint32_t> out(ret);

        DBusMethodCallHelper<decltype(in), decltype(out)> helper("app.incotool.ictcgroupd", "create_problem_cgroup");
        helper.call(client, recv, obj, in, out);

        return ret;
    }

    static std::uint32_t create_invoke_cgroup(DBusClientPtr& client, std::string recv, std::string obj,
                                              std::uint32_t prob_id, std::uint32_t owner, std::uint32_t group,
                                              std::string& name) {
        std::uint32_t ret;
        DBusMethodCallParams<std::uint32_t, std::uint32_t, std::uint32_t> in(prob_id, owner, group);
        DBusMethodCallParams<std::uint32_t, std::string> out(ret, name);

        DBusMethodCallHelper<decltype(in), decltype(out)> helper("app.incotool.ictcgroupd", "create_invoke_cgroup");
        helper.call(client, recv, obj, in, out);
        
        return ret;
    }

    static std::uint32_t remove_invoke_cgroup(DBusClientPtr& client, std::string recv, std::string obj,
                                              std::uint32_t prob_id, std::string name) {
        std::uint32_t ret;
        DBusMethodCallParams<std::uint32_t, std::string> in(prob_id, name);
        DBusMethodCallParams<std::uint32_t> out(ret);

        DBusMethodCallHelper<decltype(in), decltype(out)> helper("app.incotool.ictcgroupd", "remove_invoke_cgroup");
        helper.call(client, recv, obj, in, out);

        return ret;
    }
};

}

#endif
