/* MIT License
 *
 * Copyright (c) 2017 Vasiliy Alferov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef ICTCGROUP_ICTCGROUP_H_
#define ICTCGROUP_ICTCGROUP_H_

#include <vector>
#include <string>
#include <memory>
#include <cstring>
#include <fstream>
#include <ictprob/ictprob.h>
#include <dbus-helper.h>

namespace ictcgroup {

/* forward declarations */
class incotool_cgroup;
class incotool_problem_cgroup;
class incotool_invoke_cgroup;

/* This is thrown in case ictcgroupd failed to do something. */
class ictcgroup_exception : public std::exception {
public:
    ictcgroup_exception(std::string method); 
    ~ictcgroup_exception() = default;
    virtual const char *what() const noexcept override; 
private:
    std::string method_;
    std::string what_;
};

/* This is thrown in case of any problems while writing down cgroup config */
class ictcgroup_cgroup_exception final : public ictcgroup_exception {
public:
    ictcgroup_cgroup_exception(std::string reason);
    ~ictcgroup_cgroup_exception() = default;
    const char *what() const noexcept override;
private:
    std::string what_;
};

class incotool_cgroup {
public:
    incotool_cgroup(std::shared_ptr<incotool_cgroup> parent, std::string name);
    incotool_cgroup(incotool_cgroup&&) = default;
    virtual ~incotool_cgroup() = default;

    std::string get_name() const;
    std::string get_module_path(std::string module) const;
    std::string get_property_path(std::string module, std::string property) const;
    std::string get_hierarchy_path() const;
    
    template<class T>
    void set_property_value(std::string module, std::string property, const T& value) {
        std::string filename = get_property_path(module, property);

        std::ofstream file(filename);
        if (!file)
            throw ictcgroup_cgroup_exception("could not open file " + filename + " for writing");

        file << value;
        
        file.close();
    }

    template<class T>
    T get_property_value(std::string module, std::string property) {
        std::string filename = get_property_path(module, property);

        std::ifstream file(filename);
        if (!file)
            throw ictcgroup_cgroup_exception("could not open file " + filename + " for reading");

        T value;
        file >> value;

        file.close();
        return value;
    }
protected:
    std::string name_;
    std::shared_ptr<incotool_cgroup> parent_;
};

class incotool_root_cgroup final : public incotool_cgroup {
public:
    incotool_root_cgroup();

    incotool_root_cgroup(const incotool_root_cgroup&) = delete;
    incotool_root_cgroup& operator=(const incotool_root_cgroup&) = delete;

    incotool_root_cgroup(incotool_root_cgroup&&) = default;
    incotool_root_cgroup& operator=(incotool_root_cgroup&&) = default;

    ~incotool_root_cgroup() = default;
};

class incotool_invoke_cgroup final : public incotool_cgroup {
public:
    incotool_invoke_cgroup(std::shared_ptr<incotool_problem_cgroup> prob, std::string name);
    incotool_invoke_cgroup(incotool_invoke_cgroup&&) = default;
    ~incotool_invoke_cgroup() = default;

    int get_prob_id() const;

    void add_exec_prefix(std::string& cmd, std::vector<std::string>& args) const;
    void remove(DBus::DBusClientPtr& dbus_client) const;

private:
    int prob_id_;
};

class incotool_problem_cgroup final : public incotool_cgroup {
public:
    incotool_problem_cgroup(DBus::DBusClientPtr& client, ictprob::incotool_problem& ictprob, std::shared_ptr<incotool_root_cgroup> parent);
    
    incotool_problem_cgroup(incotool_problem_cgroup&&) = default;
    incotool_problem_cgroup& operator=(incotool_problem_cgroup&&) = default;

    incotool_problem_cgroup(const incotool_problem_cgroup&) = delete;
    incotool_problem_cgroup& operator=(const incotool_problem_cgroup&) = delete;

    ~incotool_problem_cgroup() = default;

    std::uint32_t get_prob_id() const;

    friend std::shared_ptr<incotool_invoke_cgroup> create_invoke_cgroup(DBus::DBusClientPtr& client, std::shared_ptr<incotool_problem_cgroup> prob_cgroup);

private:
    std::uint32_t prob_id_;
    std::shared_ptr<incotool_invoke_cgroup> create_subgroup(DBus::DBusClientPtr& client, std::shared_ptr<incotool_problem_cgroup> prob_cgroup) const;
};

std::shared_ptr<incotool_root_cgroup> get_root_cgroup();

std::shared_ptr<incotool_problem_cgroup> create_problem_cgroup(DBus::DBusClientPtr& client, ictprob::incotool_problem& ictprob, std::shared_ptr<incotool_root_cgroup> parent);
std::shared_ptr<incotool_problem_cgroup> create_problem_cgroup(DBus::DBusClientPtr& client, ictprob::incotool_problem& ictprob);

std::shared_ptr<incotool_invoke_cgroup> create_invoke_cgroup(DBus::DBusClientPtr& client, std::shared_ptr<incotool_problem_cgroup> prob_cgroup);
std::shared_ptr<incotool_invoke_cgroup> create_invoke_cgroup(DBus::DBusClientPtr& client, ictprob::incotool_problem ictprob);

}

#endif //ICTCGROUP_ICTCGROUP_H_
