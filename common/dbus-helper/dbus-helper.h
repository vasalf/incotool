/* MIT License
 *
 * Copyright (c) 2017 Vasiliy Alferov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef DBUS_HELPER_H_
#define DBUS_HELPER_H_

#include <exception>
#include <memory>
#include <cstdint>

#include <dbus/dbus.h>

namespace DBus {

class DBusException : public std::exception {
public:
    DBusException(const char *message)
        : message_(message) {}
    virtual ~DBusException() = default;
    virtual const char *what() const noexcept {
        return message_;
    }

private:
    const char *message_;
};

class DBusErrorOccurred final : public DBusException {
public:
    DBusErrorOccurred(DBusError &err)
        : DBusException(err.message) {}
};

/* DBusConnection needs a custom deleter */
struct DBusConnectionDeleter {
    void operator()(DBusConnection *conn) {
        dbus_connection_close(conn);
    }
};

class DBusClient {
public:
    typedef std::unique_ptr<DBusConnection, DBusConnectionDeleter> DBusConnectionPtr;

    DBusClient() {} 

    ~DBusClient() {}

    DBusClient(const DBusClient&) = delete;
    DBusClient& operator=(const DBusClient&) = delete;

    void init(const char *name) {
        dbus_error_init(&err_);

        conn_.reset(dbus_bus_get(DBUS_BUS_SYSTEM, &err_));
        errchk();
        if (!conn_.get())
            throw DBusException("Couldn't setup DBus connection (conn_ == NULL)");

        int status = dbus_bus_request_name(conn_.get(), name, DBUS_NAME_FLAG_REPLACE_EXISTING, &err_);
        errchk();
        if (status != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER)
            throw DBusException("DBus: not primary owner of a name");
    }

    DBusConnectionPtr& get_connection() {
        return conn_;
    }

private:
    DBusConnectionPtr conn_;
    DBusError err_;

    void errchk() {
        if (dbus_error_is_set(&err_))
           throw DBusErrorOccurred(err_);
    }
};
typedef std::unique_ptr<DBusClient> DBusClientPtr;

// Only basic types are supported now.

template<class T>
struct DBusType {};

//TODO: DBUS_TYPE_BYTE
//TODO: DBUS_TYPE_BOOLEAN

template<>
struct DBusType<std::int16_t> {
    typedef std::int16_t cpp_type;
    static const int dbus_int_constant = DBUS_TYPE_INT16;
};

template<>
struct DBusType<std::uint16_t> {
    typedef std::uint16_t cpp_type;
    static const int dbus_int_constant = DBUS_TYPE_UINT16;
};

template<>
struct DBusType<std::int32_t> {
    typedef std::int32_t cpp_type;
    static const int dbus_int_constant = DBUS_TYPE_INT32;
};

template<>
struct DBusType<std::uint32_t> {
    typedef std::uint32_t cpp_type;
    static const int dbus_int_constant = DBUS_TYPE_UINT32;
};

template<>
struct DBusType<std::int64_t> {
    typedef std::int64_t cpp_type;
    static const int dbus_int_constant = DBUS_TYPE_INT64;
};

template<>
struct DBusType<std::uint64_t> {
    typedef std::uint64_t cpp_type;
    static const int dbus_int_constant = DBUS_TYPE_UINT64;
};

template<>
struct DBusType<double> {
    typedef double cpp_type;
    static const int dbus_int_constant = DBUS_TYPE_DOUBLE;
};

template<>
struct DBusType<std::string> {
    typedef std::string cpp_type;
    static const int dbus_int_constant = DBUS_TYPE_STRING;
};

//TODO: DBUS_TYPE_OBJECT_PATH
//TODO: DBUS_TYPE_SIGNATURE
//TODO: DBUS_TYPE_UNIX_FD

template<class T>
inline int AppendArg(DBusMessageIter &args, T& param_) {
    return dbus_message_iter_append_basic(&args, DBusType<T>::dbus_int_constant, &param_);
}

template<>
inline int AppendArg<std::string>(DBusMessageIter &args, std::string& param_) {
    const char *str = param_.c_str();
    return dbus_message_iter_append_basic(&args, DBusType<std::string>::dbus_int_constant, &str);
}

template<class T>
inline void ExtractArg(DBusMessageIter &args, T& param_) {
    dbus_message_iter_get_basic(&args, &param_);
}

template<>
inline void ExtractArg<std::string>(DBusMessageIter &args, std::string& param_) {
    char *str;
    dbus_message_iter_get_basic(&args, &str);
    param_ = str;
}

template<class T, class... Others>
class DBusMethodCallParams {
public:
    DBusMethodCallParams(T& param, Others&... others)
        : param_(param), next_(others...) {}

    void append_all(DBusMessageIter &args) {
       if (!AppendArg(args, param_))
           throw DBusException("Couldn't append argument to DBus message: out of memory\n");
       next_.append_all(args);
    }

    void extract_all(DBusMessageIter &args) {
        if (dbus_message_iter_get_arg_type(&args) != DBusType<T>::dbus_int_constant)
            throw DBusException("Result type mismatch\n");
        ExtractArg(args, param_);
        if (!dbus_message_iter_next(&args))
            throw DBusException("Too few arguments received\n");
        next_.extract_all(args);
    }

private:
    T& param_;
    DBusMethodCallParams<Others...> next_;
};

template<class T>
class DBusMethodCallParams<T> {
public:
    DBusMethodCallParams(T& param)
        : param_(param) {}

    void append_all(DBusMessageIter &args) {
       if (!AppendArg(args, param_))
           throw DBusException("Couldn't append argument to DBus message: out of memory\n");
    }

    void extract_all(DBusMessageIter &args) {
        if (dbus_message_iter_get_arg_type(&args) != DBusType<T>::dbus_int_constant)
            throw DBusException("Result type mismatch\n");
        ExtractArg(args, param_);
    }

private:
    T& param_;
};

template<class InParams, class OutParams>
class DBusMethodCallHelper {
public:
    typedef InParams in_params;
    typedef OutParams out_params;

    DBusMethodCallHelper(std::string iface, std::string method_name) 
        : iface_(iface), method_name_(method_name) {}

    void call(DBusClientPtr& client, std::string recv, std::string obj, InParams& in, OutParams& out) {
        DBusMessage *msg = dbus_message_new_method_call(recv.c_str(), obj.c_str(), iface_.c_str(), method_name_.c_str());
        if (!msg)
            throw DBusException("Couldn't create DBus message: out of memory\n");

        DBusMessageIter args;
        dbus_message_iter_init_append(msg, &args);

        in.append_all(args);

        DBusPendingCall *pending;
        if (!dbus_connection_send_with_reply(client->get_connection().get(), msg, &pending, -1))
            throw DBusException("Couldn't send DBus message: out of memory\n");
        if (!pending)
            throw DBusException("Couldn't send DBus message: out of memory\n");

        dbus_connection_flush(client->get_connection().get());

        dbus_pending_call_block(pending);

        dbus_message_unref(msg);
        msg = dbus_pending_call_steal_reply(pending); 
        if (!msg)
            throw DBusException("Couldn't receive DBus reply: reply NULL\n");
        dbus_pending_call_unref(pending);

        if (!dbus_message_iter_init(msg, &args))
            throw DBusException("Couldn't receive DBus reply: reply is empty\n");

        out.extract_all(args);
        
        dbus_message_unref(msg);
    }

private:
    std::string iface_, method_name_;
};

}

#endif //DBUS_HELPER_H_
