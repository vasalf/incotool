/* MIT License
 *
 * Copyright (c) 2017 Vasiliy Alferov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <ictprob/ictprob.h>

#include <fstream>
#include <cstddef>
#include <cassert>
#include <memory>
#include <sstream>

#include <rapidjson/document.h>

namespace ictprob {

config_exception::config_exception(std::string reason)
        : what_(reason) {
}

const char *config_exception::what() const noexcept {
    return what_.c_str();
}

static inline std::string read_whole_file(std::string filename) {
    std::ifstream f(filename);
    if (!f)
        throw config_exception("could not open file " + filename + " for reading");
    
    std::size_t size = 1024;

    std::unique_ptr<char[]> ret(new char[size + 1]);
    f.read(ret.get(), size + 1);
    f.close();

    return ret.get();
}

static inline std::uint64_t parse_memory_limit(std::string memory_limit) {
    static const std::uint64_t KiB = 1024;
    static const std::uint64_t MiB = 1024 * KiB;
    static const std::uint64_t GiB = 1024 * MiB;
    static const std::uint64_t TiB = 1024 * GiB;

    std::uint64_t ret = 0;
    std::uint64_t mul = 1;

    if (memory_limit == "")
        throw config_exception("couldn't parse the memory limit: the string is empty");

    switch (memory_limit.back()) {
    case 'K':
        mul = KiB;
        break;
    case 'M':
        mul = MiB;
        break;
    case 'G':
        mul = GiB;
        break;
    case 'T':
        mul = TiB;
        break;
    default:
        mul = 1;
    }
    memory_limit.pop_back();

    std::istringstream ss(memory_limit);
    ss >> ret;

    return ret * mul;
}

#define CHECK_CONFIG_MEMBER(document, member, type)                              \
    if (!document.HasMember(member))                                             \
        throw config_exception("arbitrary parameter " member " was not found."); \
    if (!document[member].Is##type())                                            \
        throw config_exception("parameter " member " type mismatch")

void incotool_problem::read_config(std::string filename) {
    std::string json_data = read_whole_file(filename);

    rapidjson::Document document;
    document.Parse(json_data.c_str());

    if (!document.IsObject())
        throw config_exception("config root is not a JSON object");

    CHECK_CONFIG_MEMBER(document, "problem", Object);
   
    CHECK_CONFIG_MEMBER(document["problem"], "name", String);
    config_.name = document["problem"]["name"].GetString();

    CHECK_CONFIG_MEMBER(document["problem"], "time_limit", Int);
    config_.time_limit = (std::chrono::milliseconds)(document["problem"]["time_limit"].GetInt());

    CHECK_CONFIG_MEMBER(document["problem"], "memory_limit", String);
    config_.memory_limit = parse_memory_limit(document["problem"]["memory_limit"].GetString());

    CHECK_CONFIG_MEMBER(document["problem"], "test_pat", String);
    config_.test_pat = document["problem"]["test_pat"].GetString();
}

}
