/* MIT License
 *
 * Copyright (c) 2017 Vasiliy Alferov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <ictprob/ictprob.h>

#include <experimental/filesystem> // Waiting for C++17
#include <sstream>
#include <iomanip>
#include <algorithm>

namespace ictprob {

std::string incotool_problem::get_prettified_name() const {
    std::stringstream ss;
    ss << std::setw(6) << std::setfill('0') << prob_id_; 
    return ss.str();
}

static std::string get_test_name(int test_id) {
    std::stringstream ss;
    ss << std::setw(6) << std::setfill('0') << test_id; 
    return ss.str(); 
}

incotool_problem::incotool_problem() {}

incotool_problem::incotool_problem(std::string data_dir, int prob_id) 
                 : prob_id_(prob_id) {
    //namespace fs = std::filesystem;
    namespace fs = std::experimental::filesystem;
    workdir_ = (fs::path(data_dir) / fs::path(get_prettified_name())).string();
    read_config((fs::path(workdir_) / fs::path("problem.json")).string());
}

incotool_problem::~incotool_problem() {}

int incotool_problem::get_prob_id() const {
    return prob_id_;
}

int incotool_problem::tests_num() const {
    //namespace fs = std::filesystem;
    namespace fs = std::experimental::filesystem;

    fs::path path(workdir_);
    
    fs::directory_iterator it(path);
    int ans = 0;
    while (std::find(it, fs::directory_iterator(),
                     fs::directory_entry(fs::path(get_test_name(ans))))
           != fs::directory_iterator())
        ans++;
    
    return ans;
}

incotool_test incotool_problem::get_test(int test_num) const {
    //namespace fs = std::filesystem;
    namespace fs = std::experimental::filesystem;
    incotool_test ret;
    ret.filename = (fs::path(workdir_) / fs::path(get_test_name(test_num))).string();
    return ret;
}

const incotool_problem::config& incotool_problem::get_config() const {
    return config_;
}

}
