/* MIT License
 *
 * Copyright (c) 2017 Vasiliy Alferov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#ifndef ICTPROB_ICTPROB_H_
#define ICTPROB_ICTPROB_H_

#include <string>
#include <chrono>
#include <cstdint>

namespace ictprob {

struct incotool_test {
    std::string filename;
};

class incotool_problem {
public:
    struct config {
        std::string name;
        std::chrono::milliseconds time_limit;
        std::uint64_t memory_limit;
        std::string test_pat;
    };

    incotool_problem();
    incotool_problem(std::string data_dir, int prob_id);
    ~incotool_problem();

    int get_prob_id() const;
    int tests_num() const;
    incotool_test get_test(int test_num) const;

    std::string get_prettified_name() const;

    const config& get_config() const;

private:
    config config_;
    std::string workdir_;
    int prob_id_;

    void read_config(std::string filename);
};

class config_exception final : public std::exception {
public:
    config_exception(std::string reason);
    ~config_exception() = default;
    const char *what() const noexcept override;
private:
    std::string what_;
};

}

#endif //ICTPROB_ICTPROB_H_
