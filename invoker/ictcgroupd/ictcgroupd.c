/* MIT License
 *
 * Copyright (c) 2017 Vasiliy Alferov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include <sys/capability.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <dirent.h>
#include <errno.h>

#include <dbus/dbus.h>

#define unused __attribute__((unused))

/* cgroup stuff */

struct controller_list {
    char *controller;
    struct controller_list *next;
};

struct controller_list *create_controller(const char *controller) {
    struct controller_list *res = malloc(sizeof(struct controller_list));
    
    if (!res)
        goto ret_null;
    res->controller = malloc(sizeof(char) * (strlen(controller) + 1));
    if (!res->controller)
        goto free_res;

    strcpy(res->controller, controller);
    res->next = NULL;

    return res;

  free_res:
    free(res);
  ret_null:
    return NULL;
}

void free_controller_list(struct controller_list *list) {
    while (list) {
        struct controller_list *prev;

        free(list->controller);
        prev = list;
        list = list->next;
        free(prev);
    }
}

struct controller_list *copy_controller_list(struct controller_list *from) {
    struct controller_list *res = NULL;
    struct controller_list *cur;
    struct controller_list **where = NULL;

    while (from) {
        cur = create_controller(from->controller);
        if (!cur)
            goto free_res;
        if (where)
            *where = cur;
        if (!res)
            res = cur;
        where = &cur->next;
        from = from->next;
    }
    return res;

  free_res:
    free_controller_list(res);
    return NULL;
}

struct controller_list *add_controller_to_list(struct controller_list *list, const char *controller) {
    struct controller_list *res = create_controller(controller);

    if (!res)
        return NULL;
    res->next = list;
    return res;
}

struct cgroup {
    char *name;
    struct cgroup *par;
    struct controller_list *controllers;
};

struct cgroup *create_virtual_cgroup(struct cgroup *parent, const char *name) {
    struct cgroup *res = malloc(sizeof(struct cgroup));
    
    if (!res)
        goto ret_null;
    res->name = malloc(sizeof(char) * (strlen(name) + 1));
    if (!res->name)
        goto free_res;
    strcpy(res->name, name);

    res->par = parent;
    res->controllers = NULL;

    if (parent && parent->controllers) {
        res->controllers = copy_controller_list(parent->controllers);
        if (!res->controllers)
            goto free_res_name;
    }

    return res;

  free_res_name:
    free(res->name);
  free_res:
    free(res);
  ret_null:
    return NULL;
}

void free_virtual_cgroup(struct cgroup *cgroup) {
    if (cgroup) {
        free_controller_list(cgroup->controllers);
        free(cgroup->name);
        free(cgroup);
    }
}

/* frees cgroup and all its parents */
void free_virtual_cgroup_branch(struct cgroup *cgroup) {
    while (cgroup) {
        struct cgroup *par = cgroup->par;
        free_virtual_cgroup(cgroup);
        cgroup = par;
    }
}

char *get_cgroup_hierarchy_path(struct cgroup *cgroup) {
    size_t length = 0;
    size_t depth = 0;
    size_t i, j;
    struct cgroup *cur;
    struct cgroup **branch;
    char *res;

    // determine the result length
    cur = cgroup;
    while (cur) {
        depth++;
        length += strlen(cgroup->name);
        cur = cur->par;
    }

    // copy cgroup pointers to the array (in order from parent to child)
    branch = malloc(sizeof(struct cgroup *) * depth);
    if (!branch) {
        res = NULL;
        goto ret;
    }
    cur = cgroup;
    for (i = depth - 1; i + 1 != 0; i--) {
        branch[i] = cur;
        cur = cur->par;
    }

    // create the answer string
    res = malloc(sizeof(char) * (length + 1));
    if (!res)
        goto free_branch;
    for (i = 0, j = 0; i != depth; i++) {
        res[j++] = '/';
        strcpy(res + j, branch[i]->name);
        j += strlen(branch[i]->name);
    }

  free_branch:
    free(branch);
  ret:
    return res;
}

char *get_cgroup_controller_dir_absolute_path(struct cgroup *cgroup, const char *controller) {
    const char* sys_fs_path = "/sys/fs/cgroup";
    size_t length;
    char *hierarchy_path;
    char *res = NULL;

    hierarchy_path = get_cgroup_hierarchy_path(cgroup);
    if (!hierarchy_path)
        goto ret;

    length = strlen(sys_fs_path) + (strlen(controller) + 1) + strlen(hierarchy_path);

    res = malloc(sizeof(char) * (length + 1));
    if (!res)
        goto free_hierarchy_path;

    strcpy(res, sys_fs_path);
    res[strlen(sys_fs_path)] = '/';
    strcpy(res + strlen(sys_fs_path) + 1, controller);
    strcpy(res + (strlen(sys_fs_path) + 1) + strlen(controller), hierarchy_path);

  free_hierarchy_path:
    free(hierarchy_path);
  ret:
    return res;
}

bool does_cgroup_controller_exist(struct cgroup *cgroup, const char *controller) {
    char *check_path = get_cgroup_controller_dir_absolute_path(cgroup, controller);
    DIR *dir;
    
    if (!check_path) {
        fprintf(stderr, "<3>does_cgroup_controller_exist: out of memory\n");
        return false;
    }

    dir = opendir(check_path);
    if (dir) {
        closedir(dir);
        free(check_path);
        return true;
    }
    
    if (errno == ENOENT) {
        free(check_path);
        return false;
    }

    fprintf(stderr, "<3>does_cgroup_controller_exist: opendir(\"%s\") finished with error %m\n", check_path);
    
    free(check_path);
    return false;
}

// returns true if all of the controllers are present
bool does_cgroup_exist(struct cgroup *cgroup) {
    struct controller_list *cur;

    for (cur = cgroup->controllers; cur != NULL; cur = cur->next) {
        if (!does_cgroup_controller_exist(cgroup, cur->controller))
            return false;
    }

    return true;
}

int create_cgroup_controller(struct cgroup *cgroup, const char *controller) {
    char *create_path = get_cgroup_controller_dir_absolute_path(cgroup, controller);
    int status;

    if (!create_path) {
        fprintf(stderr, "<3>create_cgroup_controller: out of memory\n");
        return 1;
    }

    status = mkdir(create_path, 0755); 
    
    if (status != 0) {
        fprintf(stderr, "<3>create_cgroup_controller: mkdir finished with error %m\n");
    }

    free(create_path);
    
    return status;
}

int create_cgroup_branch(struct cgroup *cgroup) {
    int ret = 0;
    struct controller_list *cur;

    if (cgroup->par) {
        if (ret = create_cgroup_branch(cgroup->par))
            return ret;
    }

    for (cur = cgroup->controllers; cur != NULL; cur = cur->next) {
        int status;
        status = does_cgroup_controller_exist(cgroup, cur->controller); 
        if (!status) {
            if (ret = create_cgroup_controller(cgroup, cur->controller))
                return ret;
        }
    }
    
    return ret;
}

char *get_property_file_path(const char *dir_path, const char *filename) {
    char *res = malloc(strlen(dir_path) + (1 + strlen(filename)) + 1);
    if (res) {
        strcpy(res, dir_path);
        res[strlen(dir_path)] = '/';
        strcpy(res + strlen(dir_path) + 1, filename);
    }
    return res;
}

int cgroup_controller_files_chown(struct cgroup *cgroup, const char *controller,
                                  uid_t owner, uid_t group) {
    char *dir_path;
    char *file_path;
    DIR *d;
    struct dirent *cur;
    int status = 0;

    dir_path = get_cgroup_controller_dir_absolute_path(cgroup, controller);
    if (!dir_path) {
        fprintf(stderr, "<3>cgroup_controller_files_chown: out of memory\n");
        status = 1;
        goto ret;
    }

    d = opendir(dir_path);
    if (!d) {
        fprintf(stderr, "<3>cgroup_controller_files_chown: couldn't open dir %s: %m", dir_path);
        status = 1;
        goto free_dir_path;
    }

    for (cur = readdir(d); cur != NULL; cur = readdir(d)) {
        /* Note: the following isn't standartized by POSIX and works in Glibc only.
         * See readdir(3) for the details.
         */
        if (cur->d_type == DT_REG) {
            file_path = get_property_file_path(dir_path, cur->d_name);
            if (!file_path) {
                fprintf(stderr, "<3>cgroup_controller_files_chown: out of memory\n");
                status = 1;
                goto close_d;
            }
            status = chown(file_path, owner, group);
            if (status) {
                fprintf(stderr, "<3>cgroup_controller_files_chown: couldn't chown %s: %m\n", file_path);
                free(file_path);
                goto close_d;
            }
            free(file_path);
        }
    }

  close_d:
    closedir(d);
  free_dir_path:
    free(dir_path);
  ret:
    return status;
}

int cgroup_chown(struct cgroup *cgroup, uid_t owner, uid_t group) {
    struct controller_list *cur;
    int status = 0;

    for (cur = cgroup->controllers; cur != NULL; cur = cur->next) {
        status = cgroup_controller_files_chown(cgroup, cur->controller, owner, group);
        if (status)
            return status;
    }

    return status;
}

int recursive_delete_path(const char *dir_path) {
    char *file_path;
    DIR *d;
    struct dirent *cur;
    int status = 0;

    d = opendir(dir_path);
    if (!d) {
        fprintf(stderr, "<3>recursive_delete_path: couldn't open dir %s: %m", dir_path);
        status = 1;
        goto ret;
    }

    for (cur = readdir(d); cur != NULL; cur = readdir(d)) {
        /* Note: the following isn't standartized by POSIX and works in Glibc only.
         * See readdir(3) for the details.
         */
        if (cur->d_type == DT_DIR) {
            if (strcmp(cur->d_name, ".") == 0
                || strcmp(cur->d_name, "..") == 0)
                continue;
            file_path = get_property_file_path(dir_path, cur->d_name);
            if (!file_path) {
                fprintf(stderr, "<3>recursive_delete_path: out of memory\n");
                status = 1;
                goto close_d;
            }
            recursive_delete_path(file_path);
            free(file_path);
        }
    }

    status = rmdir(dir_path);
    //fprintf(stderr, "rmdir(\"%s\")\n", dir_path);
    if (status) {
        fprintf(stderr, "<3>recursive_delete_path: couldn't remove %s: %m\n", dir_path);
        goto close_d;
    }

  close_d:
    closedir(d);
  ret:
    return status;
}

int cgroup_controller_files_recursive_remove(struct cgroup *cgroup, const char *controller) {
    char *dir_path = get_cgroup_controller_dir_absolute_path(cgroup, controller);
    int status = recursive_delete_path(dir_path);
    free(dir_path);
    return status;
}

int cgroup_recursive_remove(struct cgroup *cgroup) {
    int status = 0;
    struct controller_list *cur;

    for (cur = cgroup->controllers; cur; cur = cur->next) {
        status = cgroup_controller_files_recursive_remove(cgroup, cur->controller);
        if (status)
            return status;
    }

    return status;
}

struct cgroup *create_virtual_incotool_root_cgroup(void) {
    struct cgroup *res = create_virtual_cgroup(NULL, "incotool");

    if (!res)
        goto ret;
    
    res->controllers = add_controller_to_list(res->controllers, "memory");
    if (!res->controllers) {
        free_virtual_cgroup(res);
        res = NULL;
        goto ret;
    }
    res->controllers = add_controller_to_list(res->controllers, "cpu");
    if (!res->controllers) {
        free_virtual_cgroup(res);
        res = NULL;
        goto ret;
    }

  ret:
    return res;
}

struct cgroup *create_virtual_incotool_problem_cgroup(int prob_id) {
    char cgroup_name[7];
    struct cgroup *root = create_virtual_incotool_root_cgroup();
    if (!root)
        return NULL;
    sprintf(cgroup_name, "%06d", prob_id);
    return create_virtual_cgroup(root, cgroup_name);
}

struct cgroup *create_virtual_incotool_invoke_cgroup(struct cgroup *prob_cgroup, int subgroup_id) {
    char cgroup_name[10];
    if (!prob_cgroup)
        return NULL;
    sprintf(cgroup_name, "%d", subgroup_id);
    return create_virtual_cgroup(prob_cgroup, cgroup_name);
}

struct {
    DBusError err;
    DBusConnection *conn;
    DBusObjectPathVTable obj;
} dbus_res;

DBusHandlerResult ictcgroupd_message_function(DBusConnection *conn, DBusMessage *msg, void *user_data);

void handle_args(int argc, char *argv[]) {
    return;
}

int setup_capabilities(void) {
    cap_t current_caps;
    cap_value_t sys_admin_caps = CAP_SYS_ADMIN;
    
    current_caps = cap_get_proc();
    if (current_caps == NULL) {
        fprintf(stderr, "<3>capabilty error: couldn't get the process's capabilities\n");
        return -1;
    }

    if (cap_set_flag(current_caps, CAP_EFFECTIVE, 1, &sys_admin_caps, CAP_SET) == -1) {
        fprintf(stderr, "<3>capabilty error: couldn't setup the local variable\n");
        return -1;
    }

    if (cap_set_proc(current_caps) == -1) {
        fprintf(stderr, "<3>capabilty error: couldn't setup the CAP_SYS_ADMIN flag\n");
        return -1;
    }

    if (cap_free(current_caps) == -1) {
        fprintf(stderr, "<3>capabilty error: couldn't free the capabilities structure\n");
        return -1;
    }

    return 0;
}

void cleanup(void) {
    if (dbus_res.conn) {
        dbus_connection_unregister_object_path(dbus_res.conn, "/app/incotool/ictcgroupd");
    }
    dbus_error_free(&dbus_res.err);
    if (dbus_res.conn) {
        dbus_connection_close(dbus_res.conn);
    }
}

int dbus_setup(void) {
    int ret;

    ret = dbus_bus_request_name(dbus_res.conn, "app.incotool.ictcgroupd", DBUS_NAME_FLAG_REPLACE_EXISTING, &dbus_res.err);
    if (dbus_error_is_set(&dbus_res.err)) {
        fprintf(stderr, "<3>dbus_setup: connection error: %s\n", dbus_res.err.message);
        return 0;
    }
    if (ret != DBUS_REQUEST_NAME_REPLY_PRIMARY_OWNER) {
        fprintf(stderr, "<3>dbus_setup: not a primary owner\n");
        return 0;
    }
    
    dbus_res.obj.message_function = ictcgroupd_message_function;
    dbus_res.obj.unregister_function = NULL;

    ret = dbus_connection_try_register_object_path(dbus_res.conn, "/app/incotool/ictcgroupd", &dbus_res.obj, NULL, &dbus_res.err);
    if (dbus_error_is_set(&dbus_res.err)) {
        fprintf(stderr, "<3>dbus_setup: object registration error: %s\n", dbus_res.err.message);
        return 0;
    }

    return 1;
}

void handle_sigterm(unused int sig) {
    fprintf(stderr, "Caught SIGTERM, cleaning up\n");
    cleanup();
    exit(0);
}

void handle_sighup(unused int sig) {
    fprintf(stderr, "No configs are used by the daemon, ignoring the request\n");
}

void reply_status(DBusMessage *msg, uint32_t ui32_status) {
    DBusMessage *reply;
    DBusMessageIter args;
    dbus_uint32_t status = ui32_status;
    dbus_uint32_t serial = 0;

    reply = dbus_message_new_method_return(msg);
    dbus_message_iter_init_append(reply, &args);

    if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_UINT32, &status)) {
        fprintf(stderr, "<3>echo: couldn't reply: out of memory\n");
        goto cleanup;
    }

    if (!dbus_connection_send(dbus_res.conn, reply, &serial)) {
        fprintf(stderr, "<3>echo: couldn't reply: out of memory\n");
        goto cleanup;
    }

    dbus_connection_flush(dbus_res.conn);

  cleanup:
    dbus_message_unref(reply);
}

#define PARSE_DBUS_ARG(args, method, type, type_s, arg_i, where, arg_n, on_failure)                                     \
{                                                                                                                       \
    if (arg_i != 1) {                                                                                                   \
        if (!dbus_message_iter_next(&args)) {                                                                           \
            fprintf(stderr, method ": DBus message argument number mismatch: wanted %d, found %d\n", arg_n, arg_i - 1); \
            on_failure;                                                                                                 \
            return 0;                                                                                                   \
        }                                                                                                               \
    }                                                                                                                   \
    if (dbus_message_iter_get_arg_type(&args) != type) {                                                                \
        fprintf(stderr, method ": DBus argument %d type mismatch: wanted %s(%d), found %d\n",                           \
                        arg_i, type_s, type, dbus_message_iter_get_arg_type(&args));                                    \
        on_failure;                                                                                                     \
        return 0;                                                                                                       \
    }                                                                                                                   \
    dbus_message_iter_get_basic(&args, where);                                                                          \
}

// DBus interface:    org.freedesktop.DBus.Introspectable
// DBus method name:  Introspect
//
// Get introspection data
//
// @return  (type char*) Introspection data in XML.

const char *introspection_reply =
"<!DOCTYPE node PUBLIC \"-//freedesktop/DTD D-BUS Introspection 1.0//EN\"   \n\
\"http://www.freedesktop.org/standards/dbus/1.0/introspect.dtd\">          \n\
<node>                                                                     \n\
  <interface name=\"org.freedesktop.DBus.Introspectable\">                 \n\
    <method name=\"Introspect\">                                           \n\
      <arg name=\"data\" direction=\"out\" type=\"s\"/>                    \n\
    </method>                                                              \n\
  </interface>                                                             \n\
  <interface name=\"app.incotool.ictcgroupd\">                             \n\
    <method name=\"create_problem_cgroup\">                                \n\
      <arg name=\"prob_id\" direction=\"in\" type=\"u\"/>                  \n\
      <arg name=\"owner\" direction=\"in\" type=\"u\"/>                    \n\
      <arg name=\"group\" direction=\"in\" type=\"u\"/>                    \n\
      <arg name=\"status\" direction=\"out\" type=\"u\"/>                  \n\
    </method>                                                              \n\
    <method name=\"create_invoke_cgroup\">                                 \n\
      <arg name=\"prob_id\" direction=\"in\" type=\"u\"/>                  \n\
      <arg name=\"owner\" direction=\"in\" type=\"u\"/>                    \n\
      <arg name=\"group\" direction=\"in\" type=\"u\"/>                    \n\
      <arg name=\"status\" direction=\"out\" type=\"u\"/>                  \n\
      <arg name=\"name\" direction=\"out\" type=\"s\"/>                    \n\
    </method>                                                              \n\
    <method name=\"remove_invoke_cgroup\">                                 \n\
      <arg name=\"prob_id\" direction=\"in\" type=\"u\"/>                  \n\
      <arg name=\"name\" direction=\"in\" type=\"s\"/>                     \n\
      <arg name=\"status\" direction\"out\" type=\"u\"/>                   \n\
    </method>                                                              \n\
  </interface>                                                             \n\
</node>";

void Introspect(DBusMessage *msg) {
    DBusMessage *reply;
    DBusMessageIter args;
    dbus_uint32_t serial = 0;

    reply = dbus_message_new_method_return(msg);
    dbus_message_iter_init_append(reply, &args);

    if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &introspection_reply)) {
        fprintf(stderr, "<3>Introspect: couldn't reply: out of memory\n");
        goto cleanup;
    }

    if (!dbus_connection_send(dbus_res.conn, reply, &serial)) {
        fprintf(stderr, "<3>Introspect: couldn't reply: out of memory\n");
        goto cleanup;
    }

    dbus_connection_flush(dbus_res.conn);

  cleanup:
    dbus_message_unref(reply);
}

// DBus interface:    app.incotool.ictcgroupd
// DBus method name:  create_problem_cgroup
//
// Creates a cgroup for a problem.
//
// @param  prob_id    (type uint32_t) The problem id.
// @param  owner      (type uint32_t) Cgroup files owner UID.
// @param  group      (type uint32_t) Cgroup files owner GID.
// @return  (type uint32_t) 0 on success, 1 on failure.

uint32_t do_create_problem_cgroup(uint32_t prob_id, uid_t owner, uid_t group) {
    struct cgroup *prob_cgrp;
    int status = 0;

    prob_cgrp = create_virtual_incotool_problem_cgroup(prob_id);
    if (!prob_cgrp) {
        fprintf(stderr, "<3>do_create_problem_cgroup: out of memory\n");
        status = 1;
        goto ret;
    }
    if (create_cgroup_branch(prob_cgrp)) {
        status = 1;
        goto free_cgroups;
    }
    if (cgroup_chown(prob_cgrp, owner, group)) {
        status = 1;
        goto free_cgroups;
    }

  free_cgroups:
    free_virtual_cgroup_branch(prob_cgrp);
  ret:
    return status;
}

int expand_create_problem_cgroup_args(DBusMessage *msg,
                                      uint32_t *prob_id,
                                      uid_t *owner,
                                      uid_t *group) {
    DBusMessageIter args;

    if (!dbus_message_iter_init(msg, &args)) {
        fprintf(stderr, "<4>create_problem_cgroup: DBus message arrived with no args, ignoring the request\n");
        return 0; 
    }
    
    PARSE_DBUS_ARG(args, "create_problem_cgroup", DBUS_TYPE_UINT32, "uint32_t", 1, prob_id, 3, 0xBEEF)
    PARSE_DBUS_ARG(args, "create_problem_cgroup", DBUS_TYPE_UINT32, "uint32_t", 2, owner,   3, 0xBEEF)
    PARSE_DBUS_ARG(args, "create_problem_cgroup", DBUS_TYPE_UINT32, "uitn32_t", 3, group,   3, 0xBEEF)

    return 1;
}

void create_problem_cgroup(DBusMessage *msg) {
    uint32_t prob_id;
    uint32_t owner, group;
    uint32_t ret = 0;

    if (expand_create_problem_cgroup_args(msg, &prob_id, &owner, &group)) {
        ret = do_create_problem_cgroup(prob_id, owner, group);
    }

    reply_status(msg, ret);
}

// DBus interface:    app.incotool.ictcgroupd
// DBus method name:  create_invoke_cgroup
//
// Creates a separate cgroup for the invocation.
//
// @param  prob_id    (type uint32_t) The problem id.
// @param  owner      (type uint32_t) Cgroup files owner UID.
// @param  group      (type uint32_t) Cgroup files owner GID.
// @return  (type uint32_t) 0 on success, 1 on failure.
// @return  (type char*) Name of the subgroup

uint32_t do_create_invoke_cgroup(uint32_t prob_id, uid_t owner, uid_t group, char **name) { 
    int group_id = 0;
    struct cgroup *prob, *res;
    int ret = 0;

    prob = create_virtual_incotool_problem_cgroup(prob_id);
    if (!prob) {
        fprintf(stderr, "<3>do_create_invoke_cgroup: out of memory\n");
        return 1;
    }

    res = create_virtual_incotool_invoke_cgroup(prob, 0);
    if (!res) {
        fprintf(stderr, "<3>do_create_invoke_cgroup: out of memory\n");
        free_virtual_cgroup_branch(prob);
        return 1;
    }
    while (does_cgroup_exist(res)) {
        ++group_id;
        free_virtual_cgroup(res);
        res = create_virtual_incotool_invoke_cgroup(prob, group_id);
        if (!res) {
            fprintf(stderr, "<3>do_create_invoke_cgroup: out of memory\n");
            free_virtual_cgroup_branch(prob);
            return 1;
        }
    }

    sprintf(*name, "%d", group_id);

    if (create_cgroup_branch(res)) {
        ret = 1;
        goto free_cgroups;
    }
    if (cgroup_chown(res, owner, group)) {
        ret = 1;
        goto free_cgroups;
    }

  free_cgroups:
    free_virtual_cgroup_branch(res);

    return ret;
}

int expand_create_invoke_cgroup_args(DBusMessage *msg,
                                     uint32_t *prob_id,
                                     uid_t *owner,
                                     uid_t *group) {
    DBusMessageIter args;

    if (!dbus_message_iter_init(msg, &args)) {
        fprintf(stderr, "<4>create_invoke_cgroup: DBus message arrived with no args, ignoring the request\n");
        return 0;
    }

    PARSE_DBUS_ARG(args, "create_invoke_cgroup", DBUS_TYPE_UINT32, "uint32_t", 1, prob_id, 3, 0xBEEF)
    PARSE_DBUS_ARG(args, "create_invoke_cgroup", DBUS_TYPE_UINT32, "uint32_t", 2, owner,   3, 0xBEEF)
    PARSE_DBUS_ARG(args, "create_invoke_cgroup", DBUS_TYPE_UINT32, "uint32_t", 3, group,   3, 0xBEEF)

    return 1;
}

void reply_invoke_subgroup_creation(DBusMessage *msg, uint32_t ui32_status, char *name) {
    DBusMessage *reply;
    DBusMessageIter args;
    dbus_uint32_t status = ui32_status;
    dbus_uint32_t serial = 0;

    reply = dbus_message_new_method_return(msg);
    dbus_message_iter_init_append(reply, &args);

    if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_UINT32, &status)) {
        fprintf(stderr, "<3>create_invoke_subgroup: couldn't reply: out of memory\n");
        goto cleanup;
    }
    if (!dbus_message_iter_append_basic(&args, DBUS_TYPE_STRING, &name)) {
        fprintf(stderr, "<3>create_invoke_subgroup: couldn't reply: out of memory\n");
        goto cleanup;
    }

    if (!dbus_connection_send(dbus_res.conn, reply, &serial)) {
        fprintf(stderr, "<3>create_invoke_cgroup: couldn't reply: out of memory\n");
        goto cleanup;
    }

    dbus_connection_flush(dbus_res.conn);

  cleanup:
    dbus_message_unref(reply);
}

void create_invoke_cgroup(DBusMessage *msg) {
    uint32_t prob_id;
    uint32_t owner, group;
    char name_s[10];
    char *name = name_s;
    uint32_t ret = 0;

    if (expand_create_invoke_cgroup_args(msg, &prob_id, &owner, &group)) {
        ret = do_create_invoke_cgroup(prob_id, owner, group, &name);
    }

    reply_invoke_subgroup_creation(msg, ret, name);
}

// DBus interface:    app.incotool.ictcgroupd
// DBus method name:  remove_invoke_cgroup
//
// Deletes an invokation cgroup.
//
// @param  prob_id    (type uint32_t) The problem id.
// @param  name       (char *)        Name of cgroup to be deleted.
// @return  (type uint32_t) 0 on success, 1 on failure.

uint32_t do_remove_invoke_cgroup(uint32_t prob_id, const char *name) {
    struct cgroup *prob, *inv;
    int status;

    prob = create_virtual_incotool_problem_cgroup(prob_id);
    if (!prob)
        return 1;
    inv = create_virtual_cgroup(prob, name);
    if (!inv) {
        free_virtual_cgroup_branch(prob);
        return 1;
    }

    status = cgroup_recursive_remove(inv);

    free_virtual_cgroup_branch(inv);

    return status;
}

int expand_remove_invoke_cgroup_args(DBusMessage *msg, uint32_t *prob_id, char **name) {
    DBusMessageIter args;

    if (!dbus_message_iter_init(msg, &args)) {
        fprintf(stderr, "<3>remove_invoke_cgroup: DBus message arrived with no args, ignoring the request\n");
        return 0; 
    }
    
    PARSE_DBUS_ARG(args, "remove_invoke_cgroup", DBUS_TYPE_UINT32, "uint32_t", 1, prob_id, 2, 0xBEEF)
    PARSE_DBUS_ARG(args, "remove_invoke_cgroup", DBUS_TYPE_STRING, "char*",    2, name,    2, 0xBEEF)

    return 1;
}

void remove_invoke_cgroup(DBusMessage *msg) {
    uint32_t prob_id;
    char *name;
    uint32_t ret = 0;

    if (expand_remove_invoke_cgroup_args(msg, &prob_id, &name)) {
        ret = do_remove_invoke_cgroup(prob_id, name);
    }

    reply_status(msg, ret);
}

DBusHandlerResult ictcgroupd_message_function(DBusConnection *conn, DBusMessage *msg, unused void *user_data) {
    const char *interface_name = dbus_message_get_interface(msg);
    const char *member_name = dbus_message_get_member(msg);

    if (strcmp(interface_name, "org.freedesktop.DBus.Introspectable") == 0) {
        if (strcmp(member_name, "Introspect") == 0) {
            Introspect(msg);
            return DBUS_HANDLER_RESULT_HANDLED;
        }
    }

    if (strcmp(interface_name, "app.incotool.ictcgroupd") == 0) {
        if (strcmp(member_name, "create_problem_cgroup") == 0) {
            create_problem_cgroup(msg);
            return DBUS_HANDLER_RESULT_HANDLED;
        }
        if (strcmp(member_name, "create_invoke_cgroup") == 0) {
            create_invoke_cgroup(msg);
            return DBUS_HANDLER_RESULT_HANDLED;
        }
        if (strcmp(member_name, "remove_invoke_cgroup") == 0) {
            remove_invoke_cgroup(msg);
            return DBUS_HANDLER_RESULT_HANDLED;
        }
    }

    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED;
}

void dbus_main_loop(void) {
    while (true) {
        dbus_connection_read_write_dispatch(dbus_res.conn, 1000);
    }
}

int main(int argc, char *argv[]) {
    int init_error = 0;

    signal(SIGTERM, handle_sigterm);
    signal(SIGHUP,  handle_sighup);

    if (setup_capabilities()) {
        exit(1);
    }

    // Initialize DBus
    dbus_error_init(&dbus_res.err);
    dbus_res.conn = dbus_bus_get(DBUS_BUS_SYSTEM, &dbus_res.err);
    if (dbus_error_is_set(&dbus_res.err)) {
        fprintf(stderr, "<3>DBus connection error (%s)\n", dbus_res.err.message);
        cleanup();
        exit(1);
    }
    if (dbus_res.conn == NULL) {
        fprintf(stderr, "<3>Couldn't initialize DBus connection\n");
        cleanup();
        exit(1);
    }
    
    if (!dbus_setup()) {
        fprintf(stderr, "<3>Couldn't setup DBus name service\n");
        cleanup();
        exit(1);
    }

    dbus_main_loop();

    return 0;
}
