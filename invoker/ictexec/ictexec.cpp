/* MIT License
 *
 * Copyright (c) 2017 Vasiliy Alferov
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <string>
#include <vector>
#include <iostream>
#include <cstring>
#include <chrono>
#include <cstdint>
#include <cstddef>
#include <memory>
#include <thread>
#include <experimental/filesystem>

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>

#include <tclap/CmdLine.h>

#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/filewritestream.h>
#include <rapidjson/writer.h>
#include <rapidjson/prettywriter.h>

#include <ictprob/ictprob.h>
#include <ictcgroup/ictcgroup.h>
#include <incotool/incotool.h>

struct {
    std::string data_dir;
    std::string var_dir;
    int prob_id;
    std::string cmd_line;
    std::string cmd;
    std::vector<std::string> args;
    ictprob::incotool_problem prob;
} invocation;

struct test_results {
    enum class verdict_t {
        OK, TL, ML, RE, CF
    };
    typedef std::chrono::milliseconds time_elapsed_t;
    typedef std::uint64_t memory_used_t;

    verdict_t verdict;
    std::string cmd_line;
    pid_t child_pid;
    time_elapsed_t time_elapsed;
    memory_used_t memory_used;
    int exit_code = -1;
    int signal = -1;

    std::string prettify_verdict() const {
        switch (verdict) {
            case verdict_t::OK:
                return "OK";
            case verdict_t::TL:
                return "TL";
            case verdict_t::ML:
                return "ML";
            case verdict_t::RE:
                return "RE";
            case verdict_t::CF:
                return "CF";
        }
        return "Worse than CF";
    }
};

template<class Writer>
class JsonProxyObject {
public:
    JsonProxyObject(Writer& writer)
            : writer_(writer) {
        writer_.StartObject();
    }
    ~JsonProxyObject() {
        writer_.EndObject();
    }
private:
    Writer &writer_;
};

void write_check_failed_response(std::string reason) {
    typedef rapidjson::Writer<rapidjson::StringBuffer> Writer;
    typedef JsonProxyObject<Writer> Object;

    rapidjson::StringBuffer s;
    Writer writer(s);

    {
        Object json_root(writer);

        writer.Key("invocation");
        {
            Object json_invocation(writer);

            // verdict
            writer.Key("verdict");
            writer.String("CF");
            
            // reason
            writer.Key("reason");
            writer.String(reason.c_str());
        }
    }

    std::cout << s.GetString() << std::endl;
}

namespace rapidjson {
    template<class OutputStream>
    class NormalWriter : public Writer<StringBuffer> {
    public:
        NormalWriter(OutputStream &os) 
                : os_(os), Writer<StringBuffer>(sb) {}
        ~NormalWriter() {
            os_ << sb.GetString() << std::endl;
        }
    private:
        OutputStream &os_;
        StringBuffer sb;
    };
}

template<class Writer>
void write_response(const test_results& results, Writer &&writer) {
    typedef JsonProxyObject<Writer> Object;
    
    {
        Object json_root(writer);

        writer.Key("invocation");
        {
            Object json_invocation(writer);

            // verdict
            writer.Key("verdict");
            writer.String(results.prettify_verdict().c_str());

            // cmd_line
            writer.Key("cmd_line");
            writer.String(results.cmd_line.c_str());

            // child_pid
            writer.Key("child_pid");
            writer.Uint(results.child_pid);

            // time_elapsed
            writer.Key("time_elapsed");
            writer.Uint64(results.time_elapsed.count());

            // memory_used
            writer.Key("memory_used");
            writer.Uint64(results.memory_used);

            // exit_code
            writer.Key("exit_code");
            writer.Int(results.exit_code);

            // signal
            writer.Key("signal");
            writer.Int(results.signal);
        }
    }
}

std::string join(std::string cmd, std::vector<std::string> args) {
    std::string ans = "";
    for (std::string& s : args) {
        if (ans.size())
            ans.push_back(' ');
        std::copy(s.begin(), s.end(), std::back_inserter(ans));
    }
    return ans;
}

class rlimit_exception final : public std::exception {
public:
    rlimit_exception(std::string resource)
        : what_("couldn't set rlimit for " + resource + ": " + std::string(strerror(errno))) {}
    ~rlimit_exception() = default;
    const char *what() const noexcept override {
        return what_.c_str();
    }
private:
    std::string what_;
};

class invocation_failed_exception final : public std::exception {
public:
    invocation_failed_exception(std::string reason)
        :what_(reason) {}
    ~invocation_failed_exception() = default;
    const char *what() const noexcept override {
        return what_.c_str();
    }
private:
    std::string what_;
};

class separate_dir {
public:
    separate_dir(std::string var_dir, std::shared_ptr<ictcgroup::incotool_invoke_cgroup> cgrp) {
        namespace fs = std::experimental::filesystem;
        
        int prob_id = cgrp->get_prob_id();
        std::ostringstream prob_id_s;
        prob_id_s << prob_id;

        std::string dirname = prob_id_s.str() + "-" + cgrp->get_name();

        fs::path path = fs::path(var_dir) / fs::path(dirname);

        path_ = path.string();

        if (fs::exists(path))
            throw invocation_failed_exception("separate directory " + path_ + " already exists");
        
        std::error_code ec;
        if (!fs::create_directory(path, ec))
            throw invocation_failed_exception("couldn't create directory " + path_ + ": " + ec.message());
    }

    std::string path() const {
        return path_;
    }

    ~separate_dir() noexcept(false) {
        namespace fs = std::experimental::filesystem;

        std::error_code ec;
        if (fs::remove_all(path_) == static_cast<std::uint64_t>(-1))
            throw invocation_failed_exception("could not remove separate directory " + path_ + ": " + ec.message());
    }

private:
    std::string path_;
};

// Invoke on one test
test_results invoke_once(DBus::DBusClientPtr& dbus_client, std::string cmd, std::vector<std::string> args) {
    auto cgrp = ictcgroup::create_invoke_cgroup(dbus_client, invocation.prob);
    separate_dir exec_dir(invocation.var_dir, cgrp);
    cgrp->add_exec_prefix(cmd, args);

    std::unique_ptr<char*[]> argv(new char*[args.size() + 1]);
    for (std::size_t i = 0; i != args.size(); i++)
        // std::unique_ptr can't store `const char*`
        argv[i] = const_cast<char*>(args[i].c_str());
    argv[args.size()] = NULL;

    test_results ret;
    ret.cmd_line = join(cmd, args);

    unsigned long int rtl_seconds = std::chrono::duration_cast<std::chrono::seconds>(invocation.prob.get_config().time_limit + std::chrono::seconds(1)).count();

    pid_t pid = fork();
    if (pid < 0)
        throw invocation_failed_exception("Could not fork: " + std::string(strerror(errno)));

    if (pid == 0) {
        // child
        
        rlimit cpu_limit = {
            .rlim_cur = rtl_seconds,
            .rlim_max = rtl_seconds
        };
        if (setrlimit(RLIMIT_CPU, &cpu_limit))
            throw rlimit_exception("RLIMIT_CPU");

        rlimit nproc_limit = {
            .rlim_cur = 1,
            .rlim_max = 1
        };
        if (setrlimit(RLIMIT_NPROC, &nproc_limit))
            throw rlimit_exception("RLIMIT_NPROC");

        if (chdir(exec_dir.path().c_str()))
            throw invocation_failed_exception("Could not change dir to " + exec_dir.path() + ": " + std::string(strerror(errno)));

        execvp(cmd.c_str(), argv.get());

    } else {
        // parent
        ret.child_pid = pid;

        int exit_status;

        waitpid(pid, &exit_status, 0);

        std::chrono::nanoseconds cpuacct_usage_user(cgrp->get_property_value<std::uint64_t>("cpu", "cpuacct.usage_user"));
        ret.time_elapsed = std::chrono::duration_cast<test_results::time_elapsed_t>(cpuacct_usage_user);
        
        ret.verdict = test_results::verdict_t::OK;
        if (WIFEXITED(exit_status)) {
            ret.exit_code = WEXITSTATUS(exit_status);
            if (ret.exit_code != 0)
                ret.verdict = test_results::verdict_t::RE;
        } else if (WIFSIGNALED(exit_status)) {
            ret.signal = WTERMSIG(exit_status);
            ret.verdict = test_results::verdict_t::RE;
        }

        ret.memory_used = cgrp->get_property_value<std::uint64_t>("memory", "memory.max_usage_in_bytes");
        if (ret.memory_used > invocation.prob.get_config().memory_limit)
            ret.verdict = test_results::verdict_t::ML;

        if (ret.time_elapsed > invocation.prob.get_config().time_limit)
            ret.verdict = test_results::verdict_t::TL;
    }

    cgrp->remove(dbus_client);
    
    return ret;
}

int main(int argc, char **argv) {
    bool pretty;

    try {
        TCLAP::CmdLine cmd("Invoke an incotool submission. Executable file and cmd \
                            line arguments should be specified after \"--\".", ' ', 
                            std::string(incotool_short_version) + " " + std::string(incotool_long_version));
       
        TCLAP::ValueArg<std::string> data_dir_arg("", "data-dir", 
                                                  "Incotool data directory.",
                                                  true, "", "directory", cmd);

        TCLAP::ValueArg<std::string> var_dir_arg("", "var-dir",
                                                 "Incotool variable data directory.",
                                                 true, "", "directory", cmd);

        TCLAP::ValueArg<int> prob_id_arg("", "prob-id",
                                         "Incotool problem id.",
                                         true, -1, "id", cmd);

        TCLAP::SwitchArg pretty_arg("", "pretty", 
                                    "Write JSON response in a \"pretty\" way.", cmd);
    
        cmd.parse(argc, argv);
        invocation.data_dir = data_dir_arg.getValue();
        invocation.var_dir = var_dir_arg.getValue();
        invocation.prob_id = prob_id_arg.getValue();
        pretty = pretty_arg.getValue();

        invocation.cmd_line = "";
        char **start = NULL;
        for (int i = 0; i < argc; i++)
            if (strcmp(argv[i], "--") == 0)
                start = &argv[i];
        if (start == NULL || start == argv + argc - 1) {
            TCLAP::ArgException e("No executable file specified");
            cmd.getOutput()->failure(cmd, e);
            throw e;
        }
        for (int i = start - argv + 1; i < argc; i++)
            invocation.cmd_line += "\"" + std::string(argv[i]) + "\" ";

        invocation.cmd = *(start + 1);
        for (int i = start - argv + 1; i < argc; i++)
            invocation.args.push_back(std::string(argv[i]));
    } catch (TCLAP::ArgException &e) {
        write_check_failed_response("TCLAP::ArgException: " + std::string(e.what()));
        return 2;
    } catch (TCLAP::ExitException &e) {
        std::ostringstream ss;
        ss << e.getExitStatus();
        write_check_failed_response("TCLAP::ExitException: exit status " + ss.str());
        return 2;
    }
    
    try {
        invocation.prob = ictprob::incotool_problem(invocation.data_dir, invocation.prob_id);

        DBus::DBusClientPtr dbus_client = std::make_unique<DBus::DBusClient>();
        dbus_client->init("app.incotool.invoker");

        auto result = invoke_once(dbus_client, invocation.cmd, invocation.args);

        assert(result.verdict != test_results::verdict_t::CF);
        if (pretty) {
            typedef rapidjson::PrettyWriter<rapidjson::FileWriteStream> Writer;
            const std::size_t bufsize = 1024;
            char buf[bufsize];
            rapidjson::FileWriteStream fws(stdout, buf, sizeof(buf));
            write_response<Writer>(result, Writer(fws));
            fws.Flush();
            std::cout << std::endl;
        } else
            write_response<rapidjson::NormalWriter<decltype(std::cout)> >(result, std::cout);
    } catch (ictprob::config_exception &e) {
        write_check_failed_response("ictprob::config_exception: " + std::string(e.what()));
        return 1;
    } catch (DBus::DBusException &e) {
        write_check_failed_response("DBus::DBusException: " + std::string(e.what()));
        return 1;
    } catch (ictcgroup::ictcgroup_cgroup_exception &e) {
        write_check_failed_response("ictcgroup::ictcgroup_cgroup_exception: " + std::string(e.what()));
        return 1;
    } catch (ictcgroup::ictcgroup_exception &e) {
        write_check_failed_response("ictcgroup::ictcgroup_exception: " + std::string(e.what()));
        return 1;
    } catch (invocation_failed_exception &e) {
        write_check_failed_response("invocation failed: " + std::string(e.what()));
        return 1;
    }

    return 0;
}
