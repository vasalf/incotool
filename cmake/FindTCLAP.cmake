# From Anima-Public/Anima github project

set (TCLAP_FOUND FALSE)

if (EXISTS TCLAP_INCLUDE_DIR)
    if (TCLAP_INCLUDE_DIR)
        set (TCLAP_FOUND TRUE)
    endif (TCLAP_INCLUDE_DIR)
else (EXISTS TCLAP_INCLUDE_DIR)
    find_path (TCLAP_INCLUDE_DIR tclap/CmdLine.h
        /usr/include
        /opt/local/include
    )
    
    if (TCLAP_INCLUDE_DIR)
        set (TCLAP_FOUND TRUE)
    endif (TCLAP_INCLUDE_DIR)
endif (EXISTS TCLAP_INCLUDE_DIR)

